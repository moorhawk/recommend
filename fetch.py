import requests
import sqlite3
import os

from common.hn import get_stories_after_timestamp

db_file = "out/posts.db"

con = sqlite3.connect(db_file)

cur = con.cursor()

cur.execute("CREATE TABLE if not exists posts(id,title,url,user,site,created_at_i,description)")

res = cur.execute("select created_at_i from posts order by created_at_i desc")

date = res.fetchone()

if date is None:
    date = 0

hits = get_stories_after_timestamp(date)

data = []

for hit in hits["hits"]:
    data.append((hit["objectID"], hit["title"], hit["author"], hit["url"], "Hacker News", hit["created_at_i"], hit["story_text"]))

print("insert {} posts".format(str(len(data))))
cur.executemany("insert into posts VALUES(?,?,?,?,?,?,?)", data)

con.commit()
