from datetime import datetime
import requests
import json

template = """Post Title : {}
Submitted by : {}
On Site : {}
Links to : {}
Description :
{}
"""

def story_to_text(hit,site):
    return template.format(
            hit["title"],
            hit["author"],
            site,
            hit["url"],
            hit["story_text"]
            )

def get_stories_in_bound(lbound,rbound):
    resp = requests.get("https://hn.algolia.com/api/v1/search_by_date?tags=story&hitsPerPage=1000&attributesToRetrieve=title,author,url,story_text&numericFilters=created_at_i>={},created_at_i<={}".format(lbound, rbound))
    return json.loads(resp.text)

def get_all_stories_today(day):
    dt = datetime.today()  # Get timezone naive now
    seconds = dt.timestamp()

    hits = get_stories_in_bound(seconds - (day + 1 * 4) * 3600, seconds - (day) * 3600)["hits"]
    for i in range(5):
        hits += get_stories_in_bound(seconds - (day + (i + 2) * 4) * 3600, seconds - (day + (i + 1) * 4) * 3600)["hits"]

    return hits

def get_stories_after_timestamp(seconds):
    resp = requests.get("https://hn.algolia.com/api/v1/search_by_date?tags=story&hitsPerPage=1000&attributesToRetrieve=title,author,url,story_text,created_at_i&numericFilters=created_at_i>={}".format(seconds))
    return json.loads(resp.text)

def get_story(id):
    resp = requests.get("https://hn.algolia.com/api/v1/items/{}".format(id))
    return json.loads(resp.text)
    return hits
