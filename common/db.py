import sqlite3
import time,datetime

db_file = "../the_collector/collector.db"

template = """Post Title : {title}
Submitted by : {user}
On Site : {site}
Links to : {url}
Description :
{description}
"""

def story_to_text(post):
    return template.format(**post)

def get_all_stories():

    con = sqlite3.connect(db_file)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    res = cur.execute("select * from posts order by created_at_i desc")

    return res.fetchall()

def get_latest_stories():

    con = sqlite3.connect(db_file)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    ts = time.mktime(datetime.datetime.now().timetuple())
    ts -= 7 * 24 * 3600

    res = cur.execute("select * from posts where created_at_i >={} order by created_at_i desc".format(ts)).fetchall()

    return res
