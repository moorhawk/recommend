import sys
import spacy
import json

from common.db import get_latest_stories, get_all_stories, story_to_text

trained_nlp = spacy.load("out/model-best")

with open("assets/train.json") as fp:
    data = json.load(fp)

all_ids = {}
for ex in data["examples"]:
    all_ids[ex["id"]] = 1

hits = get_all_stories()

count = 0

for hit in hits:

    text = story_to_text(hit)
 
    doc = trained_nlp(text)

    if doc.cats["misc"] >= 0.5 and doc.cats["recommendation"] <= 0.6 and doc.cats["recommendation"] >= 0.5:
        print("Title: {}, pred: {}, url: {}".format(hit["title"], doc.cats, hit["url"]))

        choice = input("enter cat: misc = 0, reco = 1,  else skip")

        if choice == "0":
            val = {"id": hit["id"],"site": "Hacker News", "text": text, "label": "misc"}
            if val["id"] not in all_ids:
                data["examples"].append(val)
            else:
                print("already in, skip")
        elif choice == "1":
            val = {"id": hit["id"],"site": "Hacker News", "text": text, "label": "recommendation"}
            if val["id"] not in all_ids:
                data["examples"].append(val)
            else:
                print("already in, skip")
        count += 1

    if count > 100:
        break
print("parse {} stories.".format(str(len(hits))))

with open("assets/train.json", "w") as fp:
    json.dump(data, fp, sort_keys=True, indent=2)
