import sys
import os
import requests
import json

from datetime import datetime
dt = datetime.today()  # Get timezone naive now
seconds = dt.timestamp()

seconds -= 7 * 3600 # stories in the week

resp = requests.get("https://hn.algolia.com/api/v1/search_by_date?query=&tags=story&hitsPerPage=100&numericFilters=created_at_i<={}".format(seconds))

hits = json.loads(resp.text)["hits"]

for hit in hits:
    print("Title: {}".format(hit["title"]))
