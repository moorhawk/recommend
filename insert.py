import sys
import spacy
import json
import sys
from common.hn import get_story, story_to_text

trained_nlp = spacy.load("out/model-best")

with open("assets/train.json") as fp:
    data = json.load(fp)

hits = get_story(sys.argv[1])

hits["story_text"] = hits["text"]
text = story_to_text(hits, "Hacker News")
val = {"id": hits["id"],"site": "Hacker News", "text": text, "label": sys.argv[2]}
data["examples"].append(val)

with open("assets/train.json", "w") as fp:
    json.dump(data, fp, sort_keys=True, indent=2)
