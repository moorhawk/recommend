# Description

I regularly browse the internet for recommendation. I take a particular interest
in recommendations on which books to read, which blogs to read or which podcasts
to listen to. Parsing the internet can however take a lot of time. So i thought
about how to automate this. I also wanted to learn a bit about how AI training
works so i thought to use this opportunity to learn both.

In this repository you will find training data that classifies Hacker news
stories into two categories, recommendation or misc. A recommendation story
means the person in some way asked for a recommendation. This can take the form
of "what X would you recommend" or "best software that does X".

# How To train the network

```bash
python -m spacy project run all
```

# Debug Data

```bash
python -m spacy project run debug
```

# links

https://news.ycombinator.com/item?id=36391655
