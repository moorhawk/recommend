import spacy

from fastapi import FastAPI, Request
import uvicorn
from pydantic import BaseModel
from fastapi.templating import Jinja2Templates

from common.db import get_latest_stories, get_all_stories, story_to_text

class TextToAnnotate(BaseModel):
    text: str

app = FastAPI()
trained_nlp = spacy.load("out/model-best")
templates = Jinja2Templates(directory="templates/")

@app.get('/books')
def get(request: Request):
    hits = get_latest_stories()
    out = ""

    texts = [story_to_text(t) for t in hits]
    docs = list(trained_nlp.pipe(texts))
    posts = []

    for i, doc in enumerate(docs):
        
        if doc.cats["book"] < 0.8:
            continue

        tmp = {}

        for k in hits[i].keys():
            tmp[k] = hits[i][k]
        tmp["pred"] = doc.cats
        
        posts.append(tmp)
    
    return templates.TemplateResponse('index.html', context={'request': request, "results": posts})

@app.get('/course')
def get(request: Request):
    hits = get_latest_stories()
    out = ""

    texts = [story_to_text(t) for t in hits]
    docs = list(trained_nlp.pipe(texts))
    posts = []

    for i, doc in enumerate(docs):
        
        if doc.cats["course"] < 0.6:
            continue

        tmp = {}

        for k in hits[i].keys():
            tmp[k] = hits[i][k]
        tmp["pred"] = doc.cats
        
        posts.append(tmp)
    
    return templates.TemplateResponse('index.html', context={'request': request, "results": posts})

@app.get('/')
def get(request: Request):
    hits = get_all_stories()
    out = ""

    texts = [story_to_text(t) for t in hits]
    docs = list(trained_nlp.pipe(texts))
    posts = []

    for i, doc in enumerate(docs):
        
        if doc.cats["recommendation"] < 0.5:
            continue

        tmp = {}

        for k in hits[i].keys():
            tmp[k] = hits[i][k]
        tmp["pred"] = doc.cats
        
        posts.append(tmp)
    
    return templates.TemplateResponse('index.html', context={'request': request, "results": posts})

if __name__ == "__main__":
    host = '127.0.0.1'
    uvicorn.run("webhn:app", host=host, port=9999)
