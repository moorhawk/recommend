import sys
import spacy
import json

from common.db import get_latest_stories, story_to_text

trained_nlp = spacy.load("out/model-best")

with open("assets/train.json") as fp:
    data = json.load(fp)

present = {}

for ex in data["examples"]:
    if ex["id"] in present:
        print(ex)
    else:
        present[ex["id"]] = 1
