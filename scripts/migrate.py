import sys
import os
import requests
import json
import spacy
from datetime import datetime
import urllib.parse

def get_story_by_title(title):
    safe_string = urllib.parse.quote_plus(title)
    resp = requests.get("https://hn.algolia.com/api/v1/search_by_date?query={}&tags=story&hitsPerPage=1000&attributesToRetrieve=title,url".format(safe_string))
    return json.loads(resp.text)

with open("assets/train.json") as fp:
    data = json.load(fp)

for example in data["examples"]:

    hits = get_story_by_title(example["text"])

    example["site"] = "Hacker News"
    example["id"] = hits["hits"][0]["objectID"]

with open("assets/train.json", "w") as fp:
    json.dump(data, fp, sort_keys=True, indent=2)
