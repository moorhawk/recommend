import sys
import os
import requests
import json
import spacy
from datetime import datetime
import urllib.parse


template = """Post Title : {}
Submitted by : {}
On Site : {}
Links to : {}
Description :
{}
"""

def get_story_by_id(objectid):
    resp = requests.get("https://hn.algolia.com/api/v1/items/{}".format(objectid))
    return json.loads(resp.text)

def story_to_text(story):
    return template.format(
            story["title"],
            story["author"],
            example["site"],
            story["url"],
            story["text"]
            )

with open("assets/train.json") as fp:
    data = json.load(fp)

for example in data["examples"]:
    story = get_story_by_id(example["id"])
    print(example["text"])
    print("to")
    example["text"] = story_to_text(story)
    print(example["text"])

with open("assets/train.json", "w") as fp:
    json.dump(data, fp, sort_keys=True, indent=2)
