#!/bin/bash

python3 -m venv devenv
source devenv/bin/activate

pip install -U pip setuptools wheel
pip install numpy Cython
pip install thinc
pip install spacy
pip install scikit-learn
pip install fastapi
pip install uvicorn
python -m spacy download en_core_web_md
deactivate

echo "source devenv/bin/activate"
