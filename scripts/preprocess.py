import sys
import json

from typing import Set, List, Tuple
from spacy.tokens import DocBin
from sklearn.model_selection import train_test_split
import spacy

cats = ["misc", "recommendation"]

# Load spaCy pretrained model that we downloaded before
nlp = spacy.load("en_core_web_md")

# Create a function to create a spacy dataset
def make_docs(data: List[Tuple[str, str]], target_file: str, cats: Set[str]):
    docs = DocBin()
    # Use nlp.pipe to efficiently process a large number of text inputs, 
    # the as_tuple arguments enables giving a list of tuples as input and 
    # reuse it in the loop, here for the labels
    for doc, label in nlp.pipe(data, as_tuples=True):
        # Encode the labels (assign 1 the subreddit)
        for cat in cats:
            doc.cats[cat] = 1 if cat == label else 0
        docs.add(doc)
    docs.to_disk(target_file)
    return docs

with open(sys.argv[1], "r") as fp:
    examples = json.load(fp)["examples"]

data = []
for ex in examples:
    data.append((ex["text"],ex["label"]))

train, valid = train_test_split(data, test_size=0.3)

make_docs(train, "out/train.spacy", cats=cats)
make_docs(valid, "out/valid.spacy", cats=cats)
